#!/bin/sh
# This file is part of pkg.
#
# pkg is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# pkg is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with pkg.  If not, see <https://www.gnu.org/licenses/>.

import nake
import strutils
import strformat
import os

const
  version = "0.1.0"

proc generate_include_string(incs: seq[string]): string =
  for item in incs:
    result.add(fmt"--path:{item} ")

  result.strip()


proc load_long_help(name, path: string): string =
  result.add(&"--define:{name}:\"")
  try:
    for line in lines(path):
      result.add(line & "\n")

    result = result.strip()
    result.add("\"")
  except IOError:
    echo fmt"Failed to load longhelp file: {path}: {getCurrentExceptionMsg()}"
    result = "Error: Long help was not enabled at compile"


#let
#  includes = generate_include_string(@[
#    "submodules" / "untar" / "src",
#    "submodules" / "parsetoml" / "src",
#    "shared"
#  ])

task "pkg", "Build the pkg front-end client":
  direShell(nimExe, "c", fmt"--define:VERSION:{version}",
    load_long_help("PKG_LONG_HELP", "doc/longhelp/pkg.longhelp"),
    load_long_help("PKG_INSTALL_LONG_HELP", "doc/longhelp/pkg.install.longhelp"),
    load_long_help("PKG_QUERY_LONG_HELP", "doc/longhelp/pkg.query.longhelp"),
    load_long_help("PKG_SEARCH_LONG_HELP", "doc/longhelp/pkg.search.longhelp"),
    load_long_help("PKG_SYNC_LONG_HELP", "doc/longhelp/pkg.sync.longhelp"),
    load_long_help("PKG_UNINSTALL_LONG_HELP", "doc/longhelp/pkg.uninstall.longhelp"),
    joinPath("src", "client", "main.nim"))


task "pkgd", "Build the pkg server for hosting a repository":
  let longHelp = load_long_help("PKGD_LONG_HELP", "doc/longhelp/pkgd.longhelp")
  direShell(nimExe, "c", fmt"--define:VERSION:{version}",
    longHelp, joinPath("src", "server", "main.nim"))

task "copy-data", "Copy all data files and includes":
  copyDir(joinPath("lib", "pkg"), joinPath("dist", "includes", "pkg"))
  copyDir("doc", joinPath("dist", "doc"))
  copyDir("settings", joinPath("dist", "etc"))

task "init", "Initialize output directory":
  createDir("dist" / "bin")
  createDir("dist" / "includes")
  createDir("dist" / "doc")
  createDir("dist" / "etc")

task "build", "Build all components":
  runTask("init")
  runTask("pkg")
  runTask("pkgd")
  runTask("copy-data")

task "clean", "Clean all generated build-files":
  removeDir("dist")

task "dist-clean", "Clean everything. Yes":
  runTask("clean")

  removeDir("cache")
  removeFile("nakefile")
  removeDir("nimcache")

