# This file is part of pkg.
#
# pkg is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# pkg is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with pkg.  If not, see <https://www.gnu.org/licenses/>.

import json
import os
import strformat
import strutils

import common.version
import common.platform

import config

# See `doc/internals [manifest, package, and dependency] for more info`

type
  Dependency* = tuple
    name: string
    repository: string
    version: Version

  RefPackage = object
    name*: string
    desc*: string
    version*: Version
    supports*: set[Platform]
    dependencies*: seq[Dependency]

  Package* = ref RefPackage

  RefManifest = object
    name*: string
    version*: Version
    desc*: string
    size*: int
    todo: seq[string] # List of files to process

  Manifest* = ref RefManifest

proc validate(self: JsonNode, key: string, T: JsonNodeKind = JString) =
  if not self.hasKey(key): raise newException(IndexError, fmt"Missing required key: '{key}'")
  if self[key].kind != T: raise newException(ValueError, fmt"Invalid type for key: '{key}', expected {T}")

proc validate(self: JsonNode, patterns: seq[tuple[key: string, T: JsonNodeKind]]) =
  for item in patterns:
    self.validate(item.key, item.T)

proc `$`*(pkg: Package): string =
  result.add(fmt"Package {pkg.name} - {pkg.version}")
  result.add('\n')
  result.add(&"Description:\n\t{pkg.desc}")
  result.add('\n')
  if pkg.supports != {}:
    result.add("\nSupported platforms:")
    for item in pkg.supports: result.add(fmt" {$item}")
    result.add('\n')
  if pkg.dependencies.len > 0:
    result.add("\nDependencies:")
    for dep in pkg.dependencies:
      result.add(&"\n\t{dep.name} - {$dep.version}: {dep.repository}")

proc parse_packages(manifest: JsonNode, repoName: string): seq[Package] =
  result = newSeq[Package](0)

  for item in manifest["packages"]:
    item.validate(@[
      ("name", JString),
      ("description", JString),
      ("version", JString)
    ])
    var pkg = new RefPackage
    pkg.name = item["name"].getStr()
    pkg.desc = item["description"].getStr()
    pkg.version = item["version"].getStr().parse_version()

    # Handle optional supports key
    if "supports" in item:
      item.validate("supports", JArray)
      for supported in item["supports"]:
        if supported.kind != JString: raise newException(ValueError, "Invalid type for platform in supports key, expected JString")
        case supported.getStr().toLower():
          of "linux": pkg.supports.incl(linux)
          of "windows": pkg.supports.incl(windows)
          of "bsd": pkg.supports.incl(bsd)
          of "osx": pkg.supports.incl(osx)
          of "solaris": pkg.supports.incl(solaris)
          of "aix": pkg.supports.incl(aix)
          else:
            echo fmt"WARNING: invalid type: {supported}"
    else:
      pkg.supports = {linux, bsd, osx, windows}


    # Handle optional dependencies key
    if "dependencies" in item:
      item.validate("dependencies", JArray)
      for dep in item["dependencies"]:
        if dep.kind != JObject: raise newException(ValueError, "Invalid type for dependency, expected JObject")
        dep.validate(@[
          ("name", JString),
          ("version", JString),
          ("repository", JString)
        ])
        pkg.dependencies.add((
          $dep["name"],
          if dep["repository"].getStr() == "*": repoName else: dep["repository"].getStr(),
          dep["version"].getStr().parse_version()
        ))

    
    result.add(pkg)

  
  
iterator items*(self: Manifest): Package =
  if self.todo.len <= 0: raise newException(ValueError, "Error: No manifests loaded")
  for item in self.todo:
    var repoName: string
    if item[item.len-1].isDigit():
      repoName = item.extractFilename().rsplit(".", 2)[0]
    else:
      repoName = item.extractFilename().rsplit(".")[0]

    let db = json.parseFile(item)
    let packages = parsePackages(db, self.name)
    for package in packages: yield package


## Load the manifest for a given repository
proc load_manifest*(cfg: Config, repository: string): Manifest=
  result = new RefManifest
  result.todo = newSeq[string](0)
  result.name = repository

  var rootManifestFile: string

  if (cfg.cache / fmt"{repository}.json.0").fileExists():
    rootManifestFile = (cfg.cache / fmt"{repository}.json.0")
    var itr = 0
    while (cfg.cache / fmt"{repository}.json.{itr}").fileExists():
      result.todo.add(cfg.cache / fmt"{repository}.json.{itr}")
      inc(itr)

    echo fmt"Added multi-manifest for '{repository}' with {itr - 1} sub-manifests"
  else:
    rootManifestFile = fmt"{repository}.json"
    result.todo.add(cfg.cache / fmt"{repository}.json")
    echo fmt"Added manifest for '{repository}'"


  let manifest = json.parseFile(rootManifestFile)
  if manifest.kind != JObject: raise newException(ValueError, "Invalid manifest. Expected JObject")
  manifest.validate(@[
    ("version", JString),
    ("entries", JInt),
    ("packages", JArray)
  ])

  result.version = manifest["version"].getStr().parse_version()
  result.size = manifest["entries"].getInt()
