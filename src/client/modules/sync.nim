# This file is part of pkg.
#
# pkg is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# pkg is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with pkg.  If not, see <https://www.gnu.org/licenses/>.

import strformat

import ../config
import ../manifest

const
  PKG_SYNC_LONG_HELP {.strdefine.} = "Error: Long help was not enabled at compile time"

proc main*(cfg: Config, args: seq[string]) =
  if "-h" in args:
    echo PKG_SYNC_LONG_HELP

  var MANIFEST = load_manifest(cfg, "debug")
  for item in MANIFEST:
    echo $item
    echo ""
    #echo fmt"Package: {item.name} - {item.desc}"
#echo "sync"
#[
  Contains the module which synchronizes the local manifest.json to
  the server's.
]#
