# This file is part of pkg.
#
# pkg is free software: you can redistribute it and/or modify
# it under the terms of the GNU Lesser General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# pkg is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public License
# along with pkg.  If not, see <https://www.gnu.org/licenses/>.

import os
import strformat
import json
import parsecfg
import strutils

import untar

import version

#[
  This contains the implementation of the package format.
  
  A Package is a directory who's name ends in .pkg and contains a
  meta.json file, which describes the package. The rest of the files
  within it are used via scripts and pkg itself.

  There is also a PackageArchive, which will also be a directory containing
  a meta.json file, but will only contain the info needed to remove the
  package.

  XXX:
    New idea. Rather than using 3 scripts simply use one along the same
    sort of concept as nake does. Perhaps even fork nake and make small edits
    to support spitting the cache into a tmp directory along with using a
    different name for the script.
]#

type
  Dependency* = tuple
    name: string       ## Name of the dependency
    version: Version   ## Minimum version required to install

  Script = tuple
    init: string  ## Script to run before doing anything, ex: fetch git repos
    main: string  ## Script to run for the install phase, ex: copy files here
    post: string  ## Script to run after everything, ex: clean-up of build files or something

  Package = object
    name*: string       ## Name of the package
    desc*: string       ## Description of the package
    version*: Version    ## Version of the package
    license*: string    ## License of the packaged software
    script*: Script     ## Scripts to run at the different phases
    deps*: seq[string]  ## Dependencies needed to use this package
  
  PackageRef* = ref Package

# Prototypes
proc parse_package*(self: var PackageRef, file, temp: string)
######

proc init_package*: PackageRef =
  ## Generic initialization
  result = new Package
  result.deps = newSeq[string](0)

proc init_package*(file, temp: string): PackageRef =
  result = new Package
  result.deps = newSeq[string](0)
  result.parse_package(file, temp)

proc package_cleanup*(temp: string) =
  if temp.dirExists:
    removeDir(temp)


proc parse_package*(self: var PackageRef, file, temp: string) =
  # Ensure the package file even exists
  if not file.existsFile(): raise newException(IOError, fmt"Cannot load package '{file}'")


  # Step 1: Inflate the package to {temp}/<NAME>
  let archive = newTarFile(file)
  archive.extract(temp)

  let meta = parseFile(joinPath(temp, "META"))
  # Key is a required value
  if meta.hasKey("name"):
    if meta["name"].kind != JString: raise newException(ValueError, "Invalid type for key: name - expected string")
    self.name = meta["name"].getStr()
  else: raise newException(KeyError, "Missing required key: name")


  if meta.hasKey("description"):
    if meta["description"].kind != JString: raise newException(ValueError, "Invalid type for key: description - expected string")
    self.desc = meta["description"].getStr()


  if meta.hasKey("version"):
    if meta["version"].kind != JString: raise newException(ValueError, "Invalid type for key: version - expected string")
    self.version = version.from_string(meta["version"].getStr())


  if meta.hasKey("license"):
    if meta["license"].kind != JString: raise newException(ValueError, "Invalid type for key: version - expected string")
    self.license = meta["license"].getStr()


  if not meta.hasKey("script"): raise newException(KeyError, "Missing required key: script")
  if meta["script"].hasKey("init"):
    if meta["script"]["init"].kind != JString: raise newException(ValueError, "Invalid type for key: script.init - expected string")
    self.script.init = meta["script"]["init"].getStr()

  # Main script is a required value
  if meta["script"].hasKey("main"):
    if meta["script"]["main"].kind != JString: raise newException(ValueError, "Invalid type for key: script.main - expected string")
    self.script.main = meta["script"]["main"].getStr()
  else: raise newException(KeyError, "Missing required key: script.main")


  if meta["script"].hasKey("post"):
    if meta["script"]["post"].kind != JString: raise newException(ValueError, "Invalid type for key: script.post - expected string")
    self.script.post = meta["script"]["post"].getStr()


  if meta.hasKey("dependencies"):
    for item in meta["dependencies"]:
      if item.kind != JString: raise newException(ValueError, "Invalid type for dependency: expected string")
      self.deps.add(item.getStr())
