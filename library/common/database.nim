# This file is part of pkg.
#
# pkg is free software: you can redistribute it and/or modify
# it under the terms of the GNU Lesser General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# pkg is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public License
# along with pkg.  If not, see <https://www.gnu.org/licenses/>.

import os

#[
  Implement a simple database that basically reads the files in a
  dir that are all packages and returns a sequence of strings.
]#

proc read_database*(path: string): seq[string] =
  result = newSeq[string](0)
  for path in walkFiles(joinPath(path, "*.pkg")):
    result.add(path)

