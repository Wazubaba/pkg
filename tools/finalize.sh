#!/bin/sh
# This file is part of pkg.
#
# pkg is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# pkg is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with pkg.  If not, see <https://www.gnu.org/licenses/>.

# Helper script for building packages.
######################################
# This script will properly package a directory into a package.
#
# Currently it only tars and gzips --best the target directory, but
# it will ensure that should the backend compression containers be 
# changed that a package will be properly generated regardless, so
# try to use this in any automated scripts. For that purpose there is
# a -q | --quiet argument, and two different return values to
# determine where the packaging went wrong.

show_help()
{
	echo "Usage: $1 [ARGS] PACKAGE_DIRECTORY"
	echo 'Generates a valid package archive (it basically tar gzips the'
	echo 'thing with --best)'
	echo ''
	echo 'Options'
	echo '  -h,--help  - Show this help and exit'
	echo '  -q,--quiet - Disable verbose output'
}

TEMP=`getopt -o hq --long help,quiet -- "$@"`

if [ $? != 0 ]; then echo 'Failed to run getopt. Terminating' >&2; exit 1; fi

ECHO=echo

eval set -- "$TEMP"

while true; do
	case "$1" in
		'-h' | '--help')
			show_help $0
			exit 0
		;;
		'-q' | '--quiet')
			ECHO=true
			shift
		;;
		'--') shift; break;;
		*) break;;
	esac
done

# by Andreas Schw of http://www.linuxmisc.com/12-unix-shell/0b5808193f156426.htm
eval PACKAGE_DIRECTORY=\"\${$#}\"

# This might be needed eventually, dunno. For now the script seems to be working
# just fine without it...
# https://stackoverflow.com/a/27791633
#first_letter=$(printf %.1s "$PACKAGE_DIRECTORY")
#echo $first_letter
if [ $# -lt 1 ]; then
	$ECHO 'Incorrect number of arguments'
	$ECHO "Usage: $0 [ARGS] PACKAGE_DIRECTORY"
	exit 1
fi

if [ ! -d "$1" ]; then
	$ECHO 'Invalid package directory specified. Please specify a directory'
	exit 2
fi

$ECHO 'PKG package configuration finalizer tool'
$ECHO '########################################'

$ECHO "Tarring package $1..."
tar c "$PACKAGE_DIRECTORY" > "$PACKAGE_DIRECTORY.tar"

$ECHO "Gzipping package $PACKAGE_DIRECTORY.tar..."
gzip --best --stdout "$PACKAGE_DIRECTORY.tar" > "$PACKAGE_DIRECTORY.pkg"

$ECHO "Cleaning intermediate files..."
rm -rf "$PACKAGE_DIRECTORY" "$PACKAGE_DIRECTORY.tar"

$ECHO "Done!"
