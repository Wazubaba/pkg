#!/bin/sh
# This file is part of pkg.
#
# pkg is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# pkg is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with pkg.  If not, see <https://www.gnu.org/licenses/>.

# Helper script for building packages.
######################################
# This script will ask a few questions and then generate a valid
# package from a given directory. You must ensure that your
# setup works though, as it currently will not validate it for you.
#
# The reason this is a separate bash script is simply because the
# creation of a package is so trivial that it can be done easilly by
# hand.


# TODO: Try to implement some kind of automatic test via a chroot
# debug environment or something?
echo 'PKG package configuration generator tool'
echo '########################################'

echo 'Package name: '
read PACKAGE_NAME

echo 'Package Version: '
read PACKAGE_VERSION


echo 'Would you like to use an editor for the description, enter it normally, or skip? [e/s/N] '
read CHOICE

case "$CHOICE" in
	"e")
		echo 'Launching $EDITOR to edit the description...'
		$EDITOR '/tmp/PKG-pkgdesc'
		if [ ! -e '/tmp/PKG-pkgdesc' ]; then
			echo 'You provided an empty description. This is okay for local packages but'
			echo 'will not allow your package to be accepted into the main repository.'
			echo 'Would you like to continue? [y/N]'
			read SUBCHOICE

			case "$SUBCHOICE" in
				'y');;
				*)
					echo "Exiting pkg build helper"
					exit 0
					;;
			esac
		else
			PACKAGE_DESCRIPTION=`cat /tmp/PKG-pkgdesc`

			if [ -n "$PACKAGE_DESCRIPTION" ]; then
				echo 'You provided an empty description. This is okay for local packages but'
				echo 'will not allow your package to be accepted into the main repository.'
				echo 'Would you like to continue? [y/N]'
				read SUBCHOICE

				case "$SUBCHOICE" in
					'y');;
					*)
						echo "Exiting pkg build helper"
						exit 0
						;;
				esac
			fi
		fi
		;;
	"s")
		echo 'Skipping description. Please note that your package will not be'
		echo 'accepted into the master repository unless it contains one.'
		;;
	*)
		echo 'desc: '
		read PACKAGE_DESCRIPTION
		;;
esac

echo 'Generating package...'
mkdir $PACKAGE_NAME
/bin/echo '{' > "$PACKAGE_NAME/META.json"
/bin/echo -e "\t\"name\": \"$PACKAGE_NAME\"," >> "$PACKAGE_NAME/META.json"
/bin/echo -e "\t\"version\": \"$PACKAGE_VERSION\"," >> "$PACKAGE_NAME/META.json"
/bin/echo -e "\t\"description\": \"$PACKAGE_DESCRIPTION\"," >> "$PACKAGE_NAME/META.json"
/bin/echo -e "" >> "$PACKAGE_NAME/META.json"
/bin/echo -e '\t"script": {' >> "$PACKAGE_NAME/META.json"
/bin/echo -e '\t\t"init": "init.sh",' >> "$PACKAGE_NAME/META.json"
/bin/echo -e '\t\t"main": "main.sh",' >> "$PACKAGE_NAME/META.json"
/bin/echo -e '\t\t"post": "post.sh",' >> "$PACKAGE_NAME/META.json"
/bin/echo -e '\t},' >> "$PACKAGE_NAME/META.json"
/bin/echo -e '\t"dependencies: []"' >> "$PACKAGE_NAME/META.json"
/bin/echo -e '}' >> "$PACKAGE_NAME/META.json"
echo 'done.'

echo 'Please ensure your scripts are configured correctly!'
echo ''
echo 'After you are finished configuring your package please run'
echo '`util/finalize.sh <package_name>`.'
