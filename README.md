# P K G - A less sucky package manager
## Introduction
`pkg` is a package manager designed to not suck as much.[^¹]

My general goals are to keep the project as simple as I possibly can without
sacrificing features or performance as best I can, all whilst providing
good-enough documentation to ensure future developers can maintain this
bastard even if I disappear.

Another goal is ensuring that all file formats are human-readable and
even human-editable[^²].

Lastly, there is also the goal of trying to keep the api open enough
that future alternative clients can be made, whilst also leveraging the
awesome cross-platform capabilities of nim to ensure support of a wide-range
of operating systems.

Right now the immediate goals for support are in the following order:
1. Linux [^³]
2. BSD [^³]
3. Windows [^⁴]
4. OSX [^⁵]

[^¹]: Please note that this project is not associated with the suckless folks.
[^²]: I use json for most data-type things and toml for configuration.
[^³]: Until I switch to either open or net bsd...
[^⁴]: Tested via ReactOS. 10 is unknown
[^⁵]: I lack any experience or knowledge regarding supporting OSX


## Current state
Right now this thing is in pieces, and most documentation is scatter-brained.
We are not even remotely near version 1.0 yet, but I at least currently have
a clear path forward.


## License
The main project is licensed as GPLv3, whilst the common library code is
licensed as LGPLv3. All submodules obviously retain their own licenses.


## Dependencies
* [nim >= 0.18.1](nim-lang.org)
* [nake >= 1.9.3](http://fowlmouth.github.io/nake/)

*NOTE*: You can install nake easilly via nimble: `nimble install nake`

After that, all the dependencies are included as submodules, so ensure you
run `git update --init --recursive` before attempting to build.

Currently the submodules are as follows:
* [untar](https://github.com/dom96/untar)
* [parsetoml](https://github.com/NimParsers/parsetoml)


## Compiling
Execute nake in the root directory after aquiring all dependencies to get a
list of all available tasks. You can also build the documentation separately
by switching to the `doc` directory and running nake there in much the same
way.


## Documentation
All documentation is written in coherent formats so as to be easy to use for
reference without building. After being built you will have html-based content
for all markdown documentation and man pages for all asciidoc pages.
Documentation is compiled via [pandoc](https://pandoc.org).

Please note that I will sooner switch to a different format than rape my docs
with shitty html. This is non-negotiable.


## Contributing/Reporting bugs + a free semi-rant
Please note that I am not in even the wildest redefinition of the term
involved with or aligned with SJWs. Any calls for stupidity from that crowd
will be laughed at, rediculed, and rejected - generally in that order. I will
not be altering the current code of conduct - unless I find or am provided one
that is even more amusing - other than to remove it entirely. If your personal
political ideals lean towards that side I do not care so long as you do not
infect my project with your brand of zealotry. Code is code, stupid is stupid.

I am by no means a professional programmer nor a community representative -
merely some lunatic on the internet autistic enough to create this project.
I can guarentee it is not perfect, but I aim to at least make it usable enough
that it will serve my purposes and documented enough that anyone with ability
can bloody tweak it.


## Contacting the dev(s)
Please see `AUTHORS.md` for relevant contact info.

