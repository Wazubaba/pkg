#!/bin/sh
# This file is part of pkg.
#
# pkg is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# pkg is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with pkg.  If not, see <https://www.gnu.org/licenses/>.

# This nakefile handles compiling all documentation. It is separated for
# clarity.
#
# All documentation is built via pandoc: https://pandoc.org

import nake
import os
import strformat

task "init", "Build initial directory structure":
  createDir("generated")
  createDir("generated" / "man")
  createDir("generated" / "reference")
  createDir("generated" / "usage")

task "manpages", "Build the manpages from asciidoc sources":
  if not "generated".dirExists:
    runTask("init")
  echo "TODO: implement"

task "reference", "Build extended reference documentation":
  if not "generated".dirExists:
    runTask("init")

  echo "Building reference documentation"

  let path = "generated" / "reference"
  for kind, item in "reference".walkDir():
    if kind in [pcFile, pcLinkToFile]:
      let output = item.changeFileExt("html").extractFilename()
      echo fmt"Generating {item}->{output}..."
      direShell("pandoc", fmt"-o{path / output}", item)

task "usage", "Build usage documentation":
  if not "generated".dirExists:
    runTask("init")
  echo "TODO: implement"

task "build", "Build all documentation":
  runTask("usage")
  runTask("reference")
  runtask("manpages")

task "clean", "Clean build files":
  removeDir("generated")

task "dist-clean", "Clean everything doc-related. Yes":
  runTask("clean")

  removeFile("nakefile")
  removeDir("nimcache")

