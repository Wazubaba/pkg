## Version information
A version regarding pkg consists of HEADER-MAJOR.MINOR.PATCH-FOOTER.
An example of this is `pre-1.3.2-debug`.

The valid separators are: `-`, `.`, `,`, and `<SPACE>`

A few notes:
1. The header must not start with a number, and is optional.
2. The footer is optional and can begin with a number
3. If the version cannot be parsed properly then it will consist
   entirely of a header.

For the use of dependencies, one should omit the header and footer:
`pre-1.3.2-debug` becomes `1.3.2`. This might be changed in the future.

You can omit everything but the MAJOR octet, with the other octets
being defaulted to 0 and the header & footer being left empty.

## Manifest internals
The contents of a manifest are a json-formatted tree where the root
is a table containing some meta information and an array called `packages`
which contains tables bearing the package meta information.

The format is intended to be generally self-descriptive.

Absolute minimal example:
```json
{
	"version": "version of the manifest format(SEE VERSION SECTION)",
	"entries": 1,
	"packages": [
		{
			"name": "Name of the package",
			"description": "Description of the package",
			"version": "Version of the package"
		}
	]
}
```
This example strips all information that can be assumed default:
1. This package supports all major platforms[^1]
2. This package has no dependencies


[^1]: linux, bsd, osx, and windows

The other information in this file[^2] can be summarised as follows:
* `version`: Used as a way to handle future changes to the format
* `entries`: Number of packages within the manifest, including all
             sub-manifests.

[^2]: the outer `version` and `entries` tags

For a more complete example:
```json
{
	"version": "version of the manifest format(SEE VERSION SECTION)",
	"entries": 1,
	"packages": [
		{
			"name": "Name of the package",
			"description": "Description of the package",
			"version": "Version of the package(SEE VERSION SECTION)",
			"supports": ["linux", "bsd", "windows", "solaris"],
			"dependencies": [
				{
					"name": "name of the package",
					"version": "Minimum version",
					"repository": "*"
				}
			]
		}
	]
}
```
Note that the two omitted keys are arrays.
The `supports` key contains a list of platforms which this package
can support.

The current possible platforms:
* linux
* osx
* bsd
* windows
* solaris (possibly...)
* aix (possibly...)

The above example demonstrates a package that does not support osx
or aix, but specifies support for solaris.

Currently solaris and AIX are considered not supported because as of
the time of this software and documentation being written the os
world is in flux what with the recent CoCking of the linux kernel.

Time will tell if these operating systems get more support...

Note in the dependencies there is a single entry.
A dependency contains:
1. name - The package name that will be installed
2. version - the minimum version required to be installed
3. repository - the repository this dependency is located in - *
                denotes the same repository that the package is in.
                