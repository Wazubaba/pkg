# Manifests
idea. subdivide the manifest into multiple files. As it stands
we can subdivide the manifest on a per-repository basis (as a perk of this
we can also remove the repository key) and even more than that perhaps
even subdivide further into sets of packages.

Alternatively, we can stop using bloody json and swap to sqlite3. This would
free a huge amount of IO and improve performance extensively.

A repository as configured for the server is a directory that contains
various tar-gzipped files ending in `.pkg` along with a `manifest.json`
file. This file contains high-level meta data of the various packages
for use by the client.

A client will `sync` their local `manifest.json`, with the `path`
key being replaced by the `repository` key which will denote which
configured repository contains this package, should multiple
repositories be configured. This will likely be taken care of by an
included cron-tab script (TBD), though will also be able to be
manually invoked, of course.

# Descriptions
The short description for a package will be stored within the manifest,
but the long description will be stored as `PATH.desc`, so
```json
path: "sample.pkg"
```
will make the long description file be `sample.desc`, which will be
a flat-text file. This will not be a required file, so should the
long description file not exist the short description will be
displayed instead.

# User interactions
If a client attempts to process a package not defined within the
local manifest it should offer to re-sync and then attempt again.

All download operations should offer a `--sync` argument to perform
a resync first before continuing the module's function(s)
